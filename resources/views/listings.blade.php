@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Business Listings</div>

                 <ul class="panel-body">

                    @if(count($listings))
                        <ul class="list-group"></ul>
                            @foreach($listings as $listing)
                            <li class="list-group-item"><a href="/listings/{{$listing->id}}">{{$listing->name}}</a> </li>
                            @endforeach
                 </ul>
                        @else
                            <p>There are no Business Listing Found.</p>

                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">{{$listing->name}}<a href="/listings" class="pull-right btn btn-warning btn-xs">Back to Listings</a></div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">Address: {{$listing->address}}</li>
                        <li class="list-group-item">Website: <a target="_blank" href="http://{{$listing->website}}" >{{$listing->website}}</a></li>
                        <li class="list-group-item">email: {{$listing->email}}</li>
                        <li class="list-group-item">phone: {{$listing->phone}}</li>
                        <hr>
                        <div class="well">Description: {{$listing->bio}}</div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection